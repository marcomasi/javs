'use strict';

angular.module('javsApp')
.constant("baseURL","http://localhost:3000/")
.service('garageService', ['$resource', 'baseURL', function($resource,baseURL) {

  this.getModels = function(){
    return $resource(baseURL+"models/:id");
  };

}])

.factory('feedbackFactory', ['$resource', 'baseURL', function($resource,baseURL) {

  var feedfac = {};

  feedfac.getFeedback = function(){
    return $resource(baseURL+"feedback/:id");
  };

  return feedfac;

}])

;
