var translationsEN = {
    HEADER: {
        MENU:{
            ABOUT: 'About us',
            GARAGE: 'Garage',
            CONTACT: 'Contact'
        },
        REGISTER_NL_PH: 'Register to our Newsletter',
        BUTTON_LANG_IT: 'Italian',
        BUTTON_LANG_EN: 'English'
    },
    HOME:{
        LATEST_MODELS: 'Latest Models',
        OUR_WORKFLOW: 'Our Workflow'
    },
    ABOUTUS:{
        TITLE: 'About us',
        CONTENT: "<p>As slot car enthusiasts, we all had the pleasure to try a certain number of cars of various kinds, from modern to classics, from street cars to prototypes. However I always noticed, as probably some of you did, a lack of choice when it comes to highly detailed single seaters pleasurably performing on track, especially considering the 80s -90s period. For those, like me, remembering those races and their heroes with a bit of nostalgia, this lack is something it would be good to be fixed.</p><p>During last years, various issues prevented this dream from coming true, until the great diffusion of 3d printing technology allowed also the people with relatively little possibilities to translate their efforts in an actual object.</p><p>The purpose of this project will be producing classic single seaters from the 70s - 90s era that have a place in enthusiasts’ memories, for the exploits of their skilled drivers, for their effectiveness as racing machines, and for their iconic appearance.</p><p>We will want to know your opinion, not only about the models we will issue, but also on the models you would like us to do. Be ready to be part of this project then!"
    },
    FOOTER: {
        CONTACT_US: 'Contact Us'
    }
};

var translationsIT = {
    HEADER: {
        MENU:{
            ABOUT: 'Chi siamo',
            GARAGE: 'Garage',
            CONTACT: 'Contatti'
        },
        REGISTER_NL_PH: 'Iscriviti alla Newsletter',
        BUTTON_LANG_IT: 'Italiano',
        BUTTON_LANG_EN: 'Inglese'
    },
    HOME:{
        LATEST_MODELS: 'Ultimi Modelli',
        OUR_WORKFLOW: 'Come Lavoriamo'
    },
    ABOUTUS:{
        TITLE: 'Chi siamo',
        CONTENT: "<p>Da appassionati di slot abbiamo tutti avuto il piacere di provare più o meno auto di varie tipologie, dalle moderne alle classiche, dalle stradali ai prototipi. Tuttavia ho sempre notato, come forse alcuni di voi, una certa mancanza di offerta di monoposto ad alto dettaglio che fossero anche piacevolmente utilizzabili in pista, Specialmente per il periodo anni 80 - anni 90. Per chi, come me, ricorda le corse di quegli anni e i loro protagonisti con una punta di nostalgia, si tratta di un vuoto che sarebbe piacevole colmare.</p><p>Negli anni varie difficoltà hanno fatto sì che il progetto rimanesse un sogno nel cassetto, fino a che l’introduzione su larga scala della stampa 3d non ha consentito anche a chi ha possibilità relativamente limitate di tradurre i propri sforzi in un oggetto concreto.</p><p>Lo scopo di questo progetto sarà quello di realizzare monoposto classiche dagli anni 70 agli anni 90 che abbiano lasciato un ricordo nel cuore degli appassionati, sia per le imprese sportive dei loro piloti, sia per la loro efficacia come macchine da corsa, sia per il loro aspetto iconico.</p><p>Vorremo conoscere la vostra opinione, tanto sui modelli che faremo, quanto su quelli che vorreste che facessimo, quindi siate pronti a far parte del nostro progetto!</p>",
    },
    FOOTER: {
        CONTACT_US: 'Contattaci'
    }
};