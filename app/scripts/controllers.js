'use strict';

angular.module('javsApp')

        .controller('LanguageController', ['$translate', '$scope', '$rootScope', function ($translate, $scope, $rootScope) {

                $rootScope.lang = $translate.use();

                $scope.changeLanguage = function (langKey) {
                    $translate.use(langKey);
                };

                $rootScope.$on('$translateChangeSuccess', function (event, data) {
                    var language = data.language;

                    $rootScope.lang = language;
                });

            }])

        .controller('ContactController', ['$scope', function ($scope) {

                $scope.feedback = {mychannel: "", firstName: "", lastName: "", agree: false, email: ""};

                var channels = [{value: "tel", label: "Tel."}, {value: "Email", label: "Email"}];

                $scope.channels = channels;
                $scope.invalidChannelSelection = false;

            }])

        .controller('NewsletterController', ['$scope', function ($scope) {

                $scope.feedback = {mychannel: "", firstName: "", lastName: "", agree: false, email: ""};

                var channels = [{value: "tel", label: "Tel."}, {value: "Email", label: "Email"}];

                $scope.channels = channels;
                $scope.invalidChannelSelection = false;

            }])

        .controller('FeedbackController', ['$scope', 'feedbackFactory', function ($scope, feedbackFactory) {

                $scope.sendFeedback = function () {

                    console.log($scope.feedback);

                    if ($scope.feedback.agree && ($scope.feedback.mychannel === "")) {
                        $scope.invalidChannelSelection = true;
                        console.log('incorrect');
                    } else {
                        $scope.invalidChannelSelection = false;

                        // save server
                        feedbackFactory.getFeedback().save($scope.feedback);

                        $scope.feedback = {mychannel: "", firstName: "", lastName: "", agree: false, email: ""};
                        $scope.feedback.mychannel = "";
                        $scope.feedbackForm.$setPristine();
                        console.log($scope.feedback);
                    }
                };
            }])

        // implement the IndexController and About Controller here
        .controller('IndexController', ['$scope', 'garageService', function ($scope, garageService) {

                $scope.showModels = false;
                $scope.message = "Loading ...";
                $scope.models = garageService.getModels().query()
                        .$promise.then(
                                function (response) {
                                    $scope.models = response;
                                    $scope.showModels = true;
                                },
                                function (response) {
                                    $scope.message = "Error: " + response.status + " " + response.statusText;
                                }
                        );

            }])

        .controller('AboutController', ['$scope', function ($scope) {



            }])

        .controller('GarageController', ['$scope', 'garageService', function ($scope, garageService) {

                $scope.tab = 1;
                $scope.filtText = '';
                $scope.showDetails = false;
                $scope.showGarage = false;
                $scope.message = "Loading ...";
                garageService.getModels().query(
                        function (response) {
                            $scope.models = response;
                            $scope.showGarage = true;
                        },
                        function (response) {
                            $scope.message = "Error: " + response.status + " " + response.statusText;
                        });

                $scope.select = function (setTab) {
                    $scope.tab = setTab;

                    if (setTab === 2) {
                        $scope.filtText = "appetizer";
                    } else if (setTab === 3) {
                        $scope.filtText = "mains";
                    } else if (setTab === 4) {
                        $scope.filtText = "dessert";
                    } else {
                        $scope.filtText = "";
                    }
                };

                $scope.isSelected = function (checkTab) {
                    return ($scope.tab === checkTab);
                };

                $scope.toggleDetails = function () {
                    $scope.showDetails = !$scope.showDetails;
                };
            }])

        .controller('ModelDetailController', ['$scope', '$stateParams', 'garageService', function ($scope, $stateParams, garageService) {

                $scope.showModel = false;
                $scope.message = "Loading ...";
                $scope.model = garageService.getModels().get({id: parseInt($stateParams.id, 10)})
                        .$promise.then(
                                function (response) {
                                    $scope.model = response;
                                    $scope.showModel = true;
                                },
                                function (response) {
                                    $scope.message = "Error: " + response.status + " " + response.statusText;
                                }
                        );

            }])

        ;
