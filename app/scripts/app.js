'use strict';

angular.module('javsApp', ['ui.router', 'ngResource', 'pascalprecht.translate'])
        .config(function ($stateProvider, $urlRouterProvider, $translateProvider) {
            $stateProvider

                    // route for the home page
                    .state('app', {
                        url: '/',
                        views: {
                            'header': {
                                templateUrl: 'views/header.html',
                            },
                            'content': {
                                templateUrl: 'views/home.html',
                                controller: 'IndexController'
                            },
                            'footer': {
                                templateUrl: 'views/footer.html',
                            }
                        }

                    })

                    // route for the aboutus page
                    .state('app.aboutus', {
                        url: 'aboutus',
                        views: {
                            'content@': {
                                templateUrl: 'views/aboutus.html',
                                controller: 'AboutController'
                            }
                        }
                    })

                    // route for the garage page
                    .state('app.garage', {
                        url: 'garage',
                        views: {
                            'content@': {
                                templateUrl: 'views/garage.html',
                                controller: 'GarageController'
                            }
                        }
                    })

                    // route for the modeldetail page
                    .state('app.modeldetail', {
                        url: 'garage/:id',
                        views: {
                            'content@': {
                                templateUrl: 'views/modeldetail.html',
                                controller: 'ModelDetailController'
                            }
                        }
                    })

                    // route for the contactus page
                    .state('app.contactus', {
                        url: 'contactus',
                        views: {
                            'content@': {
                                templateUrl: 'views/contactus.html',
                                controller: 'ContactController'
                            }
                        }
                    });

            $urlRouterProvider.otherwise('/');

            $translateProvider.translations('en', translationsEN);

            $translateProvider.translations('it', translationsIT);

            $translateProvider.preferredLanguage('en');

            // Enable escaping of HTML
            $translateProvider.useSanitizeValueStrategy('escape');

        })
        ;
